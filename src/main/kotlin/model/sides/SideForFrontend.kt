package model.sides

import model.CombatType
import model.rows.Row
import model.rows.RowForFrontend

class SideForFrontend(side: Side) {
    val playerNumber = side.player
    var rowsForFrontend: Map<CombatType, RowForFrontend> = getRowsForFrontend(side.rows)

    private fun getRowsForFrontend(rows: Map<CombatType, Row>): Map<CombatType, RowForFrontend> {
        return rows.entries.map { entry -> entry.key to RowForFrontend(entry.value) }.toMap()
    }

    fun getSideScore(): Int {
        return rowsForFrontend.values.sumBy { it.getRowScore() }
    }

    override fun toString(): String {
        if (playerNumber == 2) {
            return rowsForFrontend.entries.reversed().joinToString { "$it\n" }
        }
        return rowsForFrontend.entries.joinToString { "$it\n" }
    }
}
