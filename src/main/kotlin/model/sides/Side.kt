package model.sides

import model.CombatType
import model.cards.units.UnitCard
import model.powers.Power
import model.rows.BaseRow
import model.rows.Row
import java.util.*

class Side(val player: Int) {
    val rows: MutableMap<CombatType, Row> = mutableMapOf(
            CombatType.CLOSE to BaseRow(CombatType.CLOSE),
            CombatType.RANGED to BaseRow(CombatType.RANGED),
            CombatType.SIEGE to BaseRow(CombatType.SIEGE)
    )
    val discardPile: Stack<UnitCard> = Stack()

    fun newRound() {
        for (row in rows.values) {
            discardPile.addAll(row.clearRow())
            row.removeDecorators()
        }
    }

    fun playCard(card: UnitCard, rowIndex: Int = 0): SideForFrontend {
        val combatType = card.getCombatTypes()[rowIndex]
        val row = rows[combatType] ?: error("$combatType row not present")
        val powers = row.playCard(card)
        rows[combatType] = executePowers(powers, row)
        return SideForFrontend(this)
    }

    private fun executePowers(powers: List<Power>, row: Row): Row {
        var newRow = row
        for (power in powers) {
            if (power.targetType == Row::class) {
                newRow = power.executePower(newRow) as Row
            }
        }
        return newRow
    }

    fun getRowsAsList(): List<Row> {
        return rows.values.toList()
    }

    fun getSideForFrontend(): SideForFrontend {
        return SideForFrontend(this)
    }
}
