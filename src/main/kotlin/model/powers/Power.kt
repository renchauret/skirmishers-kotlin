package model.powers

import java.util.*
import kotlin.reflect.KClass

interface Power {
    val providerId: UUID
    val targetType: KClass<*>

    fun executePower(target: Any): Any
}
