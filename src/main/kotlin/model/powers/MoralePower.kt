package model.powers

import model.rows.MoraleRow
import model.rows.Row
import java.lang.Exception
import java.util.*
import kotlin.reflect.KClass

class MoralePower(override val providerId: UUID) : Power {
    override val targetType: KClass<Row> = Row::class

    override fun executePower(target: Any): Row {
        if (target is Row) {
            return giveMorale(target)
        }
        throw Exception("Target $target is not of target type $targetType.")
    }

    private fun giveMorale(target: Row): Row {
        return MoraleRow(target, providerId)
    }
}
