package model.cards

enum class Faction {
    NORTHERN_REALMS, NILFGAARDIAN_EMPIRE, MONSTERS, SCOIATAEL, NEUTRAL
}
