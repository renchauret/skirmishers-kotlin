package model.cards.units

import model.powers.MoralePower
import model.powers.Power

class MoraleUnitCard(wrappedCard: UnitCard): UnitCardDecorator(wrappedCard) {
    override fun getPower(): String {
        return "morale, " + super.getPower()
    }

    override fun getDescriptiveName(): String {
        return super.getDescriptiveName() + " (morale)"
    }

    override fun playCard(): List<Power> {
        val powers = super.playCard()
        return powers + MoralePower(this.getId())
    }
}
