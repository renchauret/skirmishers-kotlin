package model.cards.units

class BondUnitCard(wrappedCard: UnitCard): UnitCardDecorator(wrappedCard) {
    override fun getPower(): String {
        return "bond, " + super.getPower()
    }

    override fun getDescriptiveName(): String {
        return super.getDescriptiveName() + " (bond)"
    }

    override fun getStrength(): Int {
        var baseStrength = super.getStrength()
        if (getLeftNeighbor()?.getName() == this.getName()) {
            baseStrength *= 2
        }
        if (getRightNeighbor()?.getName() == this.getName()) {
            baseStrength *= 2
        }
        return baseStrength
    }
}
