package model.cards.units

import java.util.*

class UnitCardForFrontend(unitCard: UnitCard) {
    val cardId: UUID = unitCard.getId()
    val power: String? = unitCard.getPower()
    var descriptiveName: String = unitCard.getDescriptiveName()
    var strength: Int = unitCard.getStrength()

    override fun toString(): String {
        return "{$descriptiveName ($strength)}"
    }
}
