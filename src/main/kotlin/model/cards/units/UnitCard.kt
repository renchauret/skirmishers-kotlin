package model.cards.units

import model.CombatType
import model.cards.Faction
import model.powers.Power
import java.util.*

interface UnitCard {
    fun getId(): UUID
    fun getName(): String
    fun getDescriptiveName(): String
    fun getFaction(): Faction
    fun getCombatTypes(): List<CombatType>

    fun getBaseStrength(): Int
    fun getStrength(): Int
    fun getPower(): String?
    fun getCardForFrontend(): UnitCardForFrontend
    fun playCard(): List<Power>

    fun hasPrevious(): Boolean
    fun hasNext(): Boolean
    fun getLeftNeighbor(): UnitCard?
    fun getRightNeighbor(): UnitCard?
    fun setLeftNeighbor(leftNeighbor: UnitCard)
    fun setRightNeighbor(rightNeighbor: UnitCard)
    fun insertCardBeforeThis(newLeftNeighbor: UnitCard)
    fun insertCardAfterThis(newRightNeighbor: UnitCard)
}