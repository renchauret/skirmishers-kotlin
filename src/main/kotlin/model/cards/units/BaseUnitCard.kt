package model.cards.units

import model.CombatType
import model.cards.Faction
import model.powers.Power
import java.util.*

class BaseUnitCard(
        private val name: String,
        private val faction: Faction,
        private val combatTypes: List<CombatType>,
        private val strength: Int = 0
): UnitCard {
    private val cardId: UUID = UUID.randomUUID()
    private var leftNeighborCard: UnitCard? = null
    private var rightNeighborCard: UnitCard? = null

    override fun getId(): UUID {
        return cardId
    }

    override fun getName(): String {
        return name
    }

    override fun getDescriptiveName(): String {
        return "$name $combatTypes"
    }

    override fun getFaction(): Faction {
        return faction
    }

    override fun getCombatTypes(): List<CombatType> {
        return combatTypes
    }

    override fun getBaseStrength(): Int {
        return strength
    }

    override fun getStrength(): Int {
        return strength
    }

    override fun getPower(): String? {
        return null
    }

    override fun getCardForFrontend(): UnitCardForFrontend {
        return UnitCardForFrontend(this)
    }

    override fun playCard(): List<Power> {
        return mutableListOf()
    }

    override fun hasPrevious(): Boolean {
       return (leftNeighborCard !== null)
    }

    override fun hasNext(): Boolean {
        return (rightNeighborCard !== null)
    }

    override fun getLeftNeighbor(): UnitCard? {
        return leftNeighborCard
    }

    override fun getRightNeighbor(): UnitCard? {
        return rightNeighborCard
    }

    override fun setLeftNeighbor(leftNeighbor: UnitCard) {
        leftNeighborCard = leftNeighbor
    }

    override fun setRightNeighbor(rightNeighbor: UnitCard) {
        rightNeighborCard = rightNeighbor
    }

    override fun insertCardBeforeThis(newLeftNeighbor: UnitCard) {
        if (leftNeighborCard !== null) {
            newLeftNeighbor.setLeftNeighbor(leftNeighborCard!!)
        }
        newLeftNeighbor.setRightNeighbor(this)
        leftNeighborCard = newLeftNeighbor
    }

    override fun insertCardAfterThis(newRightNeighbor: UnitCard) {
        if (rightNeighborCard !== null) {
            newRightNeighbor.setRightNeighbor(rightNeighborCard!!)
        }
        newRightNeighbor.setLeftNeighbor(this)
        rightNeighborCard = newRightNeighbor
    }
}
