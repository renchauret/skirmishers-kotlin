package model.cards.units

import model.CombatType
import model.cards.Faction
import model.powers.Power
import java.util.*

open class UnitCardDecorator(
        protected var card: UnitCard
): UnitCard {
    override fun getId(): UUID {
        return this.card.getId()
    }

    override fun getName(): String {
        return this.card.getName()
    }

    override fun getDescriptiveName(): String {
        return this.card.getDescriptiveName()
    }

    override fun getFaction(): Faction {
        return card.getFaction()
    }

    override fun getCombatTypes(): List<CombatType> {
        return card.getCombatTypes()
    }

    override fun getBaseStrength(): Int {
        return this.card.getBaseStrength()
    }

    override fun getStrength(): Int {
        return this.card.getStrength()
    }

    override fun getPower(): String? {
        return this.card.getPower()
    }

    override fun getCardForFrontend(): UnitCardForFrontend {
        return UnitCardForFrontend(this)
    }

    override fun playCard(): List<Power> {
        return this.card.playCard()
    }

    override fun hasPrevious(): Boolean {
        return this.card.hasPrevious()
    }

    override fun hasNext(): Boolean {
        return this.card.hasNext()
    }

    override fun getLeftNeighbor(): UnitCard? {
        return this.card.getLeftNeighbor()
    }

    override fun getRightNeighbor(): UnitCard? {
        return this.card.getRightNeighbor()
    }

    override fun setLeftNeighbor(leftNeighbor: UnitCard) {
        this.card.setLeftNeighbor(leftNeighbor)
    }

    override fun setRightNeighbor(rightNeighbor: UnitCard) {
        this.card.setRightNeighbor(rightNeighbor)
    }

    override fun insertCardBeforeThis(newLeftNeighbor: UnitCard) {
        this.card.insertCardBeforeThis(newLeftNeighbor)
    }

    override fun insertCardAfterThis(newRightNeighbor: UnitCard) {
        this.card.insertCardAfterThis(newRightNeighbor)
    }
}