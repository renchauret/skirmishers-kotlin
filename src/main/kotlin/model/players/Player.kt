package model.players

import model.sides.Side
import model.cards.units.UnitCardForFrontend
import model.cards.units.UnitCard
import model.rows.Row
import model.sides.SideForFrontend
import java.util.*

abstract class Player(private val number: Int, private val deck: Stack<UnitCard>) {
    private val STARTING_HAND_SIZE: Int = 10
    protected val MAX_RETURN: Int = 2

    private var wins: Int = 0
    protected var hasPassed: Boolean = false
    protected val side: Side = Side(number)
    protected val hand: MutableList<UnitCard> = mutableListOf()
    var opponent: Player? = null

    init {
        newGame()
    }

    private fun shuffleDeck() {
        this.deck.shuffle()
    }

    fun newGame() {
        newRound()
        shuffleDeck()
        for (i in 1..STARTING_HAND_SIZE) {
            this.hand.add(deck.pop())
        }
        replaceCards()
    }

    fun newRound() {
        side.newRound()
        hasPassed = false
    }

    private fun replaceCards() {
        for (i in 1..MAX_RETURN) {
            val line: Int = chooseCardToReplace(i)
            if (line > 0 && line <= hand.size) {
                val card = hand.removeAt(line - 1)
                hand.add(deck.pop())
                deck.push(card)
            } else {
                break
            }
        }
    }

    protected abstract fun chooseCardToReplace(cardNum: Int): Int

    abstract fun playCard(): SideForFrontend

    fun endRound(didWin: Boolean, anotherRound: Boolean) {
        if (didWin) {
            wins += 1
        }
        if (anotherRound) {
            newRound()
        }
    }

    protected fun printHand() {
        val handToPrint = mutableListOf<UnitCardForFrontend>()
        for (card in this.hand) {
            handToPrint.add(card.getCardForFrontend())
        }
        println("Player $number's hand:")
        println(handToPrint)
    }

    fun hasPassed(): Boolean {
        return hasPassed
    }

    fun getSideForFrontend(): SideForFrontend {
        return side.getSideForFrontend()
    }

    fun getHandSize(): Int {
        return hand.size
    }

    fun getWins(): Int {
        return wins
    }

    fun setNewOpponent(opponent: Player) {
        this.opponent = opponent
        opponent.opponent = this
    }
}
