package model.players

import model.cards.units.UnitCard
import model.sides.SideForFrontend
import java.util.*

class AiPlayer(val number: Int, private val deck: Stack<UnitCard>): Player(number, deck) {
    override fun chooseCardToReplace(cardNum: Int): Int {
        return 0
    }

    override fun playCard(): SideForFrontend {
        if (hand.size == 0 || shouldPass()) {
            hasPassed = true
            return side.getSideForFrontend()
        }
        val card = hand.removeAt(0)
        return side.playCard(card)
    }

    private fun shouldPass(): Boolean {
        val sideForFrontend = getSideForFrontend()
        val score = sideForFrontend.getSideScore()
        val opponentSideForFrontend = opponent?.getSideForFrontend()
        val opponentScore = opponentSideForFrontend?.getSideScore()

        if (opponentScore != null) {
            if (opponentScore >= score + 10 || score >= opponentScore + 10) {
                return true
            }
        }

        return false
    }
}
