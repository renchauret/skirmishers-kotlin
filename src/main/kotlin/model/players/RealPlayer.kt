package model.players

import model.cards.units.UnitCard
import model.rows.Row
import model.sides.SideForFrontend
import java.lang.NumberFormatException
import java.util.*

class RealPlayer(val number: Int, private val deck: Stack<UnitCard>): Player(number, deck) {
    override fun chooseCardToReplace(cardNum: Int): Int {
        printHand()
        println("Pick card $cardNum of $MAX_RETURN to replace")
        println("Type the index of the card to replace (first card is 1)")
        println("Enter anything else to keep this hand")
        return try {
            readLine()!!.toInt()
        } catch (nfe: NumberFormatException) {
            println("Player $number opted to keep their hand")
            0
        }
    }

    override fun playCard(): SideForFrontend {
        if (hand.size == 0) {
            hasPassed = true
            return side.getSideForFrontend()
        }
        printHand()
        println("Type the index of the card to play (first card is 1)")
        println("Enter anything else to pass this round")
        var line = 0
        try {
            line = readLine()!!.toInt()
        } catch (nfe: NumberFormatException) {}
        if (line > 0 && line <= hand.size) {
            val card = hand.removeAt(line - 1)
            val combatTypes = card.getCombatTypes()
            if (combatTypes.size > 1) {
                var row: Int = 0
                while (row < 1 || row > combatTypes.size) {
                    println("Type the index of the row in which to play this card")
                    var i = 1
                    for (combatType in combatTypes) {
                        if (i == combatTypes.size) {
                            println("($i) $combatType")
                        } else {
                            print("($i) $combatType, ")
                        }
                        i++
                    }
                    try {
                        row = readLine()!!.toInt()
                    } catch (nfe: NumberFormatException) {
                        println("Not a number")
                        continue
                    }
                }
                return side.playCard(card, row - 1)
            }
            return side.playCard(card)
        } else {
            println("Player $number passed")
            hasPassed = true
            return side.getSideForFrontend()
        }
    }
}
