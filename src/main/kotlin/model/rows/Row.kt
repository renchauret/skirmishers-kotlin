package model.rows

import model.CombatType
import model.cards.units.UnitCardForFrontend
import model.cards.units.UnitCard
import model.powers.Power
import java.util.*

interface Row {
    fun getId(): UUID
    fun getCombatType(): CombatType
    fun getDescriptiveName(): String

    fun getCardsForFrontend(): List<UnitCardForFrontend>
    fun playCard(newCard: UnitCard): List<Power>
    fun clearRow(): List<UnitCard>
    fun removeDecorators(): Row
}
