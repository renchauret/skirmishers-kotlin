package model.rows

import model.cards.units.UnitCardForFrontend
import java.util.*

class MoraleRow(wrappedRow: Row, private val moraleProviderId: UUID): RowDecorator(wrappedRow) {
    override fun getCardsForFrontend(): List<UnitCardForFrontend> {
        val baseCards: List<UnitCardForFrontend> = super.getCardsForFrontend()
        return baseCards.map {
            if (it.cardId != this.moraleProviderId) {
                it.strength += 1
            }
            it
        }
    }
}
