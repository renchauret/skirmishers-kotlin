package model.rows

import model.CombatType
import model.cards.units.UnitCardForFrontend
import model.cards.units.UnitCard
import model.powers.Power
import java.util.*

class BaseRow(
        private val combatType: CombatType,
): Row {
    private val rowId: UUID = UUID.randomUUID()
    private var card: UnitCard? = null

    override fun getId(): UUID {
        return rowId
    }

    override fun getCombatType(): CombatType {
        return combatType
    }

    override fun getDescriptiveName(): String {
        return combatType.toString()
    }

    override fun getCardsForFrontend(): List<UnitCardForFrontend> {
        if (card === null) {
            return listOf()
        }

        resetCardReference()
        var tempCardRef = card!!

        val cards = mutableListOf(tempCardRef.getCardForFrontend())
        while (tempCardRef.hasNext()) {
            tempCardRef = tempCardRef.getRightNeighbor()!!
            cards.add(tempCardRef.getCardForFrontend())
        }
        return cards
    }

    override fun playCard(newCard: UnitCard): List<Power> {
        if (card === null) {
            card = newCard
            return newCard.playCard()
        }

        resetCardReference()
        var tempCardRef = card!!

        if (tempCardRef.getName() == newCard.getName()) {
            tempCardRef.insertCardAfterThis(newCard)
            return newCard.playCard()
        }

        while (tempCardRef.hasNext()) {
            tempCardRef = tempCardRef.getRightNeighbor()!!
            if (tempCardRef.getName() == newCard.getName()) {
                tempCardRef.insertCardAfterThis(newCard)
                return newCard.playCard()
            }
        }
        tempCardRef.insertCardAfterThis(newCard)
        return newCard.playCard()
    }

    override fun clearRow(): List<UnitCard> {
        if (card === null) {
            return listOf()
        }

        val cards = mutableListOf<UnitCard>()
        var tempCardRef = card!!
        cards.add(tempCardRef)

        while (tempCardRef.hasNext()) {
            tempCardRef = tempCardRef.getRightNeighbor()!!
            cards.add(tempCardRef)
        }
        card = null
        return cards
    }

    override fun removeDecorators(): Row {
        return this
    }

    private fun resetCardReference() {
        if (card === null) {
            return
        }
        while (card!!.hasPrevious()) {
            card = card!!.getLeftNeighbor()
        }
    }
}
