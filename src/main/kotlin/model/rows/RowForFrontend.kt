package model.rows

import model.CombatType
import model.cards.units.UnitCardForFrontend

class RowForFrontend(row: Row) {
    val combatType: CombatType = row.getCombatType()
    val cardsForFrontend: List<UnitCardForFrontend> = row.getCardsForFrontend()
    val descriptiveName: String = row.getDescriptiveName()

    fun getRowScore(): Int {
        return cardsForFrontend.sumBy { it.strength }
    }

    override fun toString(): String {
        return "$descriptiveName: $cardsForFrontend"
    }
}
