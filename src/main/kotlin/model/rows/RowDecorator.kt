package model.rows

import model.CombatType
import model.cards.units.UnitCardForFrontend
import model.cards.units.UnitCard
import model.powers.Power
import java.util.*

open class RowDecorator(protected var row: Row): Row {
    override fun getId(): UUID {
        return row.getId()
    }

    override fun getCombatType(): CombatType {
        return row.getCombatType()
    }

    override fun getDescriptiveName(): String {
        return row.getDescriptiveName()
    }

    override fun getCardsForFrontend(): List<UnitCardForFrontend> {
        return row.getCardsForFrontend()
    }

    override fun playCard(newCard: UnitCard): List<Power> {
        return row.playCard(newCard)
    }

    override fun clearRow(): List<UnitCard> {
        return row.clearRow()
    }

    override fun removeDecorators(): Row {
        return row
    }
}