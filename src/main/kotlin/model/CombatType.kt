package model

enum class CombatType {
    CLOSE, RANGED, SIEGE
}
