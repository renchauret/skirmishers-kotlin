package model

import model.players.Player

class Game(val player1: Player, val player2: Player) {
    val FIRST_TO = 2

    fun playGame() {
        while (player1.getWins() < FIRST_TO && player2.getWins() < FIRST_TO) {
            playRound()
        }
        if (player1.getWins() >= FIRST_TO) {
            println("Player 1 wins the game!")
        } else if (player2.getWins() >= FIRST_TO) {
            println("Player 2 wins the game!")
        }
    }

    fun playRound() {
        while (!player1.hasPassed() || !player2.hasPassed()) {
            if (!player1.hasPassed()) {
                printBoard()
                player1.playCard()
            }
            if (!player2.hasPassed()) {
                printBoard()
                player2.playCard()
            }
        }
        if (player1.getSideForFrontend().getSideScore() > player2.getSideForFrontend().getSideScore()) {
            println("Player 1 wins the round!")
            player1.endRound(true, true)
            player2.endRound(false, true)
        } else if (player2.getSideForFrontend().getSideScore() > player1.getSideForFrontend().getSideScore()) {
            println("Player 2 wins the round!")
            player1.endRound(false, true)
            player2.endRound(true, true)
        } else {
            println("This round is a draw!")
            player1.endRound(false, true)
            player2.endRound(false, true)
        }
        println("Rounds won:  Player 1: ${player1.getWins()}, Player 2: ${player2.getWins()}")
    }

    private fun printBoard() {
        println("=======The Board=======")
        println("Cards in hand:  Player 1: ${player1.getHandSize()}, Player 2: ${player2.getHandSize()}")
        println("Score:  Player 1: ${player1.getSideForFrontend().getSideScore()}, Player 2: ${player2.getSideForFrontend().getSideScore()}")
        print(player2.getSideForFrontend().toString())
        println("-----------------------")
        print(player1.getSideForFrontend().toString())
        println("=======================")
    }
}
