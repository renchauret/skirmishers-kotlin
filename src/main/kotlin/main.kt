import model.CombatType
import model.Game
import model.cards.Faction
import model.cards.units.BaseUnitCard
import model.cards.units.BondUnitCard
import model.cards.units.MoraleUnitCard
import model.cards.units.UnitCard
import model.players.AiPlayer
import model.players.RealPlayer
import model.powers.Power
import model.rows.BaseRow
import model.rows.Row
import java.util.*

fun main(args: Array<String>) {
    val player1 = RealPlayer(1, createCards())
    val player2 = AiPlayer(2, createCards())
    player1.setNewOpponent(player2)
    val game = Game(player1, player2)
    game.playGame()
}

fun createCards(): Stack<UnitCard> {
    val cards: Stack<UnitCard> = Stack()
    cards.addAll(
            listOf(
                    BaseUnitCard(
                            "grunt",
                            Faction.MONSTERS,
                            listOf(CombatType.CLOSE),
                            2
                    ),
                    BaseUnitCard(
                            "grunt",
                            Faction.MONSTERS,
                            listOf(CombatType.CLOSE),
                            2
                    ),
                    BaseUnitCard(
                            "grunt",
                            Faction.MONSTERS,
                            listOf(CombatType.RANGED),
                            2
                    ),
                    BaseUnitCard(
                            "grunt",
                            Faction.MONSTERS,
                            listOf(CombatType.RANGED),
                            2
                    ),
                    BaseUnitCard(
                            "grunt",
                            Faction.MONSTERS,
                            listOf(CombatType.SIEGE),
                            2
                    ),
                    BaseUnitCard(
                            "grunt",
                            Faction.MONSTERS,
                            listOf(CombatType.SIEGE),
                            2
                    ),
                    BondUnitCard(
                            BaseUnitCard(
                                    "bonder",
                                    Faction.MONSTERS,
                                    listOf(CombatType.CLOSE),
                                    3
                            )
                    ),
                    BondUnitCard(
                            BaseUnitCard(
                                    "bonder",
                                    Faction.MONSTERS,
                                    listOf(CombatType.CLOSE),
                                    3
                            )
                    ),
                    BondUnitCard(
                            BaseUnitCard(
                                    "bonder",
                                    Faction.MONSTERS,
                                    listOf(CombatType.CLOSE),
                                    3
                            )
                    ),
                    MoraleUnitCard(
                            BaseUnitCard(
                                    "moraler",
                                    Faction.MONSTERS,
                                    listOf(CombatType.CLOSE),
                                    1
                            )
                    ),
                    MoraleUnitCard(
                            BaseUnitCard(
                                    "moraler",
                                    Faction.MONSTERS,
                                    listOf(CombatType.CLOSE),
                                    1
                            )
                    ),
                    MoraleUnitCard(
                            BaseUnitCard(
                                    "moralee",
                                    Faction.MONSTERS,
                                    listOf(CombatType.SIEGE),
                                    1
                            )
                    ),
                    MoraleUnitCard(
                            BondUnitCard(
                                    BaseUnitCard(
                                            "big bonder",
                                            Faction.MONSTERS,
                                            listOf(CombatType.RANGED),
                                            3
                                    )
                            )
                    ),
                    MoraleUnitCard(
                            BondUnitCard(
                                    BaseUnitCard(
                                            "big bonder",
                                            Faction.MONSTERS,
                                            listOf(CombatType.RANGED),
                                            3
                                    )
                            )
                    )
            )
    )
    return cards
}
